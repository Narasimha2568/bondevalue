//
//  Extension.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundview(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: CGColor) {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor
    }
    func shadow() {
        layer.masksToBounds = false
        layer.shadowColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5).cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 4.0
    }
    func openViewAnimation() {
        transform = CGAffineTransform(scaleX: 0, y: 0)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveLinear], animations: {
            self.transform = .identity
        })
    }
}

extension UIButton {
    
    func round(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: CGColor) {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor
    }
    
    func shadowBtn() {
        layer.masksToBounds = false
        layer.shadowColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5).cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 4.0
    }
    
    func enabled(enabled: Bool) {
        enabled ? (alpha = 1.0) : (alpha = 0.5)
        isEnabled = enabled
    }
}

extension UITextField {
    func allowOnlyAlphaNumericCharacters(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string) as NSString
        let result = newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
        if !result {
            return result
        }
        let acceptedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        let cs = NSCharacterSet(charactersIn: acceptedChars).inverted
        
        let range_ = string.rangeOfCharacter(from: cs)
        
        if range_ != nil {
            // We have found an invalid character, don't allow the change
            return false
        } else {
            // No invalid character, allow the change
            return true
        }
    }
}
