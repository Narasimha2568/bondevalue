//
//  OrderListCell.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class OrderListCell: UITableViewCell {

    @IBOutlet var orderType: UILabel!
    @IBOutlet var ownerName: UILabel!
    @IBOutlet var orderprice: UILabel!
    @IBOutlet var bgVw: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgVw.roundview(cornerRadius: 4.0, borderWidth: 1.0, borderColor: #colorLiteral(red: 0.7764705882, green: 0.04705882353, blue: 0.1882352941, alpha: 1).cgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUP(order: Order) {
        if let ordertype = order.order_type {
            if ordertype.lowercased() == "Sell".lowercased() {
                orderType.text = "Seller Name :"
            }else {
                orderType.text = "Buyer Name :"
            }
        }
        if let owner = order.order_ownername {
            ownerName.text = owner
        }
        if let price = order.order_price {
            orderprice.text = "Rs \(String(describing: price))"
        }
    }
}
