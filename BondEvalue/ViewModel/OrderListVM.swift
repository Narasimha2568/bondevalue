//
//  OrderListVM.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation

enum ORDERTYPE: String {
    case SELL = "Sell"
    case BUY = "Buy"
}

class OrderListVM {
    var orderList: [Order] = []
    
    init(type:ORDERTYPE , price: Int, completion: @escaping (_ success: Bool) -> Void) {
        GetOrderList(type: type, price:price ,completion: { _ in
            completion(true)
        })
    }
    
    func GetOrderList(type:ORDERTYPE , price: Int, completion: @escaping (_ success: Bool) -> Void) {
        Firebase.shared.fetchOrderList(completion: { orderList in
            self.orderList = orderList.filter { (orderObj: Order) -> Bool in
                (orderObj.order_type?.lowercased() != type.rawValue.lowercased())
                }.filter { (orderObj: Order) -> Bool in
                    (orderObj.order_price ?? 0 <= price)
                }.sorted(by: { (Order1, Order2) -> Bool in
                    Order1.order_price ?? 0 > Order2.order_price ?? 0
                })
            completion(true)
        })
    }
}
