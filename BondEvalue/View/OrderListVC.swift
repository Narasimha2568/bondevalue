//
//  ViewController.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class OrderListVC: UIViewController {
    
    @IBOutlet var orderListTblView: UITableView!
    private var viewModel: OrderListVM! {
        didSet {
            orderListTblView.reloadData()
        }
    }
    var listUpdated: Bool = false
    var type: ORDERTYPE = .BUY
    var price: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingNibs()
        viewModel = OrderListVM(type: type, price: price, completion: { _ in
            DispatchQueue.main.async {
                self.listUpdated = true
                self.orderListTblView.reloadData()
            }
        })
    }
    
    // MARK: - loading custom xibs
    func loadingNibs() {
        orderListTblView.register(UINib(nibName: "OrderListCell", bundle: nil), forCellReuseIdentifier: "OrderListCell")
        orderListTblView.rowHeight = UITableView.automaticDimension
        orderListTblView.sectionHeaderHeight = UITableView.automaticDimension
    }

}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension OrderListVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        if listUpdated {
            (viewModel.orderList.count == 0) ? (orderListTblView.backgroundColor = .clear) : (orderListTblView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        }
        return viewModel.orderList.count
    }
    
    func tableView(_: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderListTblView.dequeueReusableCell(withIdentifier: "OrderListCell", for: indexPath) as? OrderListCell
        cell?.setUP(order: viewModel.orderList[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 80
    }

}

