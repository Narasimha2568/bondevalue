//
//  Order.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation

struct Order: Codable {
    let order_ownername: String?
    let order_type: String?
    let order_price: Int?
}
