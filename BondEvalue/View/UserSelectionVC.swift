//
//  UserSelectionVC.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class UserSelectionVC: UIViewController {

    @IBOutlet var bgVw: UIView!
    @IBOutlet var buy: UIButton!
    @IBOutlet var sell: UIButton!
    @IBOutlet var priceTextFld: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.bgVw.shadow()
        self.bgVw.openViewAnimation()
        self.buy.round(cornerRadius: 4.0, borderWidth: 1.0, borderColor: #colorLiteral(red: 0.7764705882, green: 0.04705882353, blue: 0.1882352941, alpha: 0).cgColor)
        self.buy.shadowBtn()
        self.sell.round(cornerRadius: 4.0, borderWidth: 1.0, borderColor: #colorLiteral(red: 0.7764705882, green: 0.04705882353, blue: 0.1882352941, alpha: 1).cgColor)
        self.sell.shadowBtn()
        self.priceTextFld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.buy.enabled(enabled: false)
        self.sell.enabled(enabled: false)
    }

    @IBAction func buyBtnAction(_: Any) {
        moveToOrderList(type: .BUY)
    }
    
    @IBAction func sellBtnAction(_: Any) {
        moveToOrderList(type: .SELL)
    }

    private func moveToOrderList(type: ORDERTYPE) {
        self.priceTextFld.endEditing(true)
        guard let orderListView = storyboard?.instantiateViewController(withIdentifier: "OrderListVC") as? OrderListVC else {
            return
        }
        orderListView.type = type
        if let price = self.priceTextFld.text {
            orderListView.price = Int(price) ?? 0
        }
        navigationController?.pushViewController(orderListView, animated: true)
    }
}


extension UserSelectionVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 12
        let allow = textField.allowOnlyAlphaNumericCharacters(textField, shouldChangeCharactersIn: range, replacementString: string)
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return (newString.length <= maxLength) && allow
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        buy.enabled(enabled: !(self.priceTextFld.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ?? false))
        sell.enabled(enabled: buy.isEnabled)
    }
}
