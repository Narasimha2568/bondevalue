//
//  Firebase.swift
//  BondEvalue
//
//  Created by Narasimha Poluparthi on 28/10/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import CodableFirebase
import FirebaseDatabase
import Foundation

class Firebase {
    static let shared: Firebase = Firebase()
    var ref: DatabaseReference?
    var refHandle: DatabaseHandle?
    
    func fetchOrderList(completion: @escaping (_ result: [Order]) -> Void) {
        ref = Database.database().reference()
        
        var oderList = [Order]()
        
        refHandle = ref?.child("List").observe(.childAdded) { snapshot in
            
            guard let value = snapshot.value else { return }
            do {
                let model = try FirebaseDecoder().decode(Order.self, from: value)
                debugPrint(model)
                oderList.append(model)
            } catch {
                debugPrint(error)
            }
            debugPrint(oderList)
            completion(oderList)
        }
    }
}
